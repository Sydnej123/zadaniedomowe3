# Pierwsze zadanie
def greeter() -> None:
    name = input("Podaj swoje imie: ")
    if name.upper() == "FILIP":
        print("Witaj Filip.")
    else:
        print("Witaj nieznajomy.")


# Drugie zadanie
def zero_checker() -> None:
    number = input("Podaj liczbe: ")
    if float(number):
        print(number)
    else:
        print("To jest zero")


# Czwarte zadanie
class Triangle:
    def __init__(self, base: float, height: float):
        self.base = base
        self.height = height
        self.area = height * base / 2

    def __ne__(self, other):
        return self.area != other.area

    def __eq__(self, other):
        return self.area == other.area

    def __lt__(self, other):
        return self.area < other.area

    def __le__(self, other):
        return self.area <= other.area

    def __gt__(self, other):
        return self.area > other.area

    def __ge__(self, other):
        return self.area >= other.area


# Piąte zadanie
class Human:
    def __init__(self, iq: int):
        self.iq = iq

    def __ge__(self, other):
        return self.iq >= other.iq

    def __gt__(self, other):
        return self.iq > other.iq

    def __le__(self, other):
        return self.iq <= other.iq

    def __lt__(self, other):
        return self.iq < other.iq

    def __eq__(self, other):
        return self.iq == other.iq

    def __ne__(self, other):
        return self.iq != other.iq


class Employee(Human):
    def __init__(self, iq: int, salary: float):
        super(Employee, self).__init__(iq)
        self.salary = salary

    def __ge__(self, other):
        return self.salary >= other.salary

    def __gt__(self, other):
        return self.salary > other.salary

    def __le__(self, other):
        return self.salary <= other.salary

    def __lt__(self, other):
        return self.salary < other.salary

    def __eq__(self, other):
        return self.salary == other.salary

    def __ne__(self, other):
        return self.salary != other.salary


class Manager(Employee):
    def __init__(self, iq: int, salary: float, permission_level: int):
        super(Manager, self).__init__(iq, salary)
        self.permission_level = permission_level

    def __ge__(self, other):
        return self.permission_level >= other.permission_level

    def __gt__(self, other):
        return self.permission_level > other.permission_level

    def __le__(self, other):
        return self.permission_level <= other.permission_level

    def __lt__(self, other):
        return self.permission_level < other.permission_level

    def __eq__(self, other):
        return self.permission_level == other.permission_level

    def __ne__(self, other):
        return self.permission_level != other.permission_level


class Intern(Employee):
    def __init__(self, iq: int, salary: float, internship_length: int):
        super(Intern, self).__init__(iq, salary)
        self.internship_length = internship_length

    def __ge__(self, other):
        return self.internship_length >= other.internship_length

    def __gt__(self, other):
        return self.internship_length > other.internship_length

    def __le__(self, other):
        return self.internship_length <= other.internship_length

    def __lt__(self, other):
        return self.internship_length < other.internship_length

    def __eq__(self, other):
        return self.internship_length == other.internship_length

    def __ne__(self, other):
        return self.internship_length != other.internship_length


class Secretary(Employee):
    def __init__(self, iq: int, salary: float, known_languages: list):
        super(Secretary, self).__init__(iq, salary)
        self.known_languages = known_languages

    def __ge__(self, other):
        return len(self.known_languages) >= len(other.known_languages)

    def __gt__(self, other):
        return len(self.known_languages) > len(other.known_languages)

    def __le__(self, other):
        return len(self.known_languages) <= len(other.known_languages)

    def __lt__(self, other):
        return len(self.known_languages) < len(other.known_languages)

    def __eq__(self, other):
        return len(self.known_languages) == len(other.known_languages)

    def __ne__(self, other):
        return len(self.known_languages) != len(other.known_languages)


class Director(Employee):
    def __init__(self, iq: int, salary: float, percent_of_shares: float):
        super(Director, self).__init__(iq, salary)
        self.percent_of_shares = percent_of_shares

    def __ge__(self, other):
        return self.percent_of_shares >= other.percent_of_shares

    def __gt__(self, other):
        return self.percent_of_shares > other.percent_of_shares

    def __le__(self, other):
        return self.percent_of_shares <= other.percent_of_shares

    def __lt__(self, other):
        return self.percent_of_shares < other.percent_of_shares

    def __eq__(self, other):
        return self.percent_of_shares == other.percent_of_shares

    def __ne__(self, other):
        return self.percent_of_shares != other.percent_of_shares
