import random

# Zadanie 1
x = random.randrange(0, 100)
while x != 20:
    x = random.randrange(0, 100)

# Zadanie 2
for n in range(20, -1, -1):
    print(n)
